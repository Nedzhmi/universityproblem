package dss.university.model.entity;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "students")
@Where(clause = "deleted=0")
public class Student extends BaseEntity{

    @Expose
    private String firstName;
    @Expose
    private String lastName;
    @Expose
    private Long facultyNumber;
    @Expose
    private List<Double> grades;

    public Student() {
    }

    public Student(String firstName, String lastName, Long facultyNumber, List<Double> grades) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.facultyNumber = facultyNumber;
        this.grades = grades;
    }

    @Column(name = "first_name")
    @NotNull
    @Length(min = 2, max = 40)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name")
    @NotNull
    @Length(min = 2, max = 40)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "faculty_number", unique = true)
    @NotNull
    @Min(value = 1)
    public Long getFacultyNumber() {
        return facultyNumber;
    }

    public void setFacultyNumber(Long facultyNumber) {
        this.facultyNumber = facultyNumber;
    }


    @ElementCollection(fetch = FetchType.EAGER)
    @Column
    @NotNull
    @NotEmpty
    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }
}
