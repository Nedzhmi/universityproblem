package dss.university.model.dto;

import com.google.gson.annotations.Expose;

public class StudentGetRecordDto {
    @Expose
    private Long facultyNumber;

    public Long getFacultyNumber() {
        return facultyNumber;
    }

    public void setFacultyNumber(Long facultyNumber) {
        this.facultyNumber = facultyNumber;
    }

}
