package dss.university.model.dto;

import com.google.gson.annotations.Expose;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class StudentImportGradesDto {
    @Expose
    private Long facultyNumber;
    @Expose
    private Double grade;

    @NotNull
    @Min(value = 1)
    public Long getFacultyNumber() {
        return facultyNumber;
    }

    public void setFacultyNumber(Long facultyNumber) {
        this.facultyNumber = facultyNumber;
    }

    @NotNull
    @Min(value = 2)
    @Max(value = 6)
    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
