package dss.university.util;

public interface ValidationUtil {

    <E> boolean isValid(E entity);
}
