package dss.university.util;

import javax.validation.Validator;

//@Component
public class ValidationUtilImpl implements ValidationUtil {

    private final Validator validator;

//    @Autowired
    public ValidationUtilImpl(Validator validator) {
        this.validator = validator;
    }

    @Override
    public <E> boolean isValid(E entity) {
        return this.validator.validate(entity).isEmpty();
    }
}
