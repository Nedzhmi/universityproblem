package dss.university.service.impl;

import com.google.gson.Gson;
import dss.university.dao.StudentRepository;
import dss.university.model.dto.StudentExportDto;
import dss.university.model.dto.StudentExportJsonDto;
import dss.university.model.dto.StudentImportGradesDto;
import dss.university.model.dto.StudentRecordDto;
import dss.university.model.entity.Student;
import dss.university.service.StudentService;
import dss.university.util.ValidationUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

    public static final String STUDENTS_PATH = "src/main/resources/files/json/students.json";

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;
    private final Gson gson;
    private final ValidationUtil validationUtil;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, ModelMapper modelMapper, Gson gson, ValidationUtil validationUtil) {
        this.studentRepository = studentRepository;
        this.modelMapper = modelMapper;
        this.gson = gson;
        this.validationUtil = validationUtil;
    }

    public Student addStudent(Student student) {
        if (this.validationUtil.isValid(student)) {
            return this.studentRepository.saveAndFlush(student);
        }
        return null;
    }

    @Override
    public List<Student> getAll() {
        return this.studentRepository.findAll();
    }

    @Override
    public Optional<Student> getById(Long id) {
        return this.studentRepository.findById(id);
    }

    @Override
    public Optional<Student> findByFacultyNumber(Long facultyNumber) {
        return this.studentRepository.findByFacultyNumber(facultyNumber);
    }

    @Override
    @Transactional
    public boolean update(Long id, Student student) {
        Optional<Student> studentById = this.studentRepository.findById(id);
        if (studentById.isPresent()) {
            student.setId(id);
            this.studentRepository.saveAndFlush(student);
        }

        return studentById.isPresent();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        this.studentRepository.softDeleteById(id);
    }

    @Override
    @Transactional
    public boolean addGradesToStudent(StudentImportGradesDto studentImportGradesDto) {
        if (this.validationUtil.isValid(studentImportGradesDto)) {
            Optional<Student> studentByFacultyNumber = this.studentRepository
                    .findByFacultyNumber(studentImportGradesDto.getFacultyNumber());

            if (studentByFacultyNumber.isPresent()) {
                Student student = studentByFacultyNumber.get();
                student.getGrades().add(studentImportGradesDto.getGrade());
                this.studentRepository.save(student);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public List<StudentExportDto> getAllWithAverageGrade() {
        List<Student> all = this.getAll();

        List<StudentExportDto> exportDtos = new LinkedList<>();
        for (Student student : all) {
            StudentExportDto exportDto = this.modelMapper.map(student, StudentExportDto.class);
            double averageGrade = getAverageGrade(student);
            exportDto.setAverageGrade(averageGrade);

            exportDtos.add(exportDto);
        }

        return exportDtos;
    }

    @Override
    public List<StudentExportDto> getAllSortedByFacultyNumber() {
        List<StudentExportDto> students = this.getAllWithAverageGrade();

        selectionSort(students);
        return students;
    }

    private void selectionSort(List<StudentExportDto> students) {
        for (int i = 0; i < students.size() - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < students.size(); j++) {
                if(students.get(j).getFacultyNumber() < students.get(minIndex).getFacultyNumber()) {
                    minIndex = j;
                }
            }

            Collections.swap(students, minIndex, i);
//            StudentExportDto temp = students.get(minIndex);
//            StudentExportDto studentExportDto = students.get(minIndex);
//            studentExportDto = students.get(i);
//            StudentExportDto studentExportDto1 = students.get(i);
//            studentExportDto1 = temp;
        }
    }

    @Override
    public List<StudentExportDto> getAllSortedByAverageGrade() {
        List<StudentExportDto> allWithAverageGrade = this.getAllWithAverageGrade();
        StudentExportDto[] students = new StudentExportDto[allWithAverageGrade.size()];
        allWithAverageGrade.toArray(students);

        for (int i = 1; i < students.length; i++) {
            StudentExportDto key = students[i];
            int j = i - 1;

            while (j >= 0 && students[j].getAverageGrade() < key.getAverageGrade()) {
                students[j + 1] = students[j];
                j = j - 1;
            }
            students[j + 1] = key;
        }

        return Arrays.asList(students);
    }

    @Override
    public StudentRecordDto getStudentRecord(Long facultyNumber) {
        List<Student> students = this.getAll();

        List<StudentRecordDto> recordDtos = new LinkedList<>();
        for (Student student : students) {
            StudentRecordDto recordDto = this.modelMapper.map(student, StudentRecordDto.class);
            double averageGrade = getAverageGrade(student);
            recordDto.setAverageGrade(averageGrade);

            recordDtos.add(recordDto);
        }

        recordDtos.sort(Comparator.comparingLong(StudentRecordDto::getFacultyNumber));

        int index = binarySearch(recordDtos, 0, recordDtos.size() - 1, facultyNumber);

        return index > -1 ? recordDtos.get(index) : null;
    }

    @Override
    public void writeToFile() throws IOException {
        StringBuilder sb = new StringBuilder("[");
        FileWriter writer = new FileWriter(STUDENTS_PATH);

        List<Student> students = this.studentRepository.findAll();
        for (int i = 0; i < students.size(); i++) {
            if (i < students.size() - 1) {
                sb.append(this.gson.toJson(students.get(i))).append(",").append(System.lineSeparator());
            } else {
                sb.append(this.gson.toJson(students.get(i))).append(System.lineSeparator());
            }
        }

        sb.append("]");
        writer.write(sb.toString());
        writer.flush();
        writer.close();
    }

    @Override
    public List<Student> readFromFile() throws IOException {
        String strResult = String.join("", Files.readAllLines(Path.of(STUDENTS_PATH)));
        List<Student> students = new LinkedList<>();

        if (!"".equals(strResult)) {
            StudentExportJsonDto[] studentExportJsonDtos = this.gson.fromJson(strResult, StudentExportJsonDto[].class);

            if (studentExportJsonDtos.length > 0) {
                for (StudentExportJsonDto studentExportJsonDto : studentExportJsonDtos) {
                    if (this.validationUtil.isValid(studentExportJsonDto)) {
                        Student student = this.modelMapper.map(studentExportJsonDto, Student.class);
                        student.setDeleted(0);
                        students.add(student);
                    }
                }
            }
        }

        return students.size() > 0 ? students : null;
    }

    private int binarySearch(List<StudentRecordDto> recordDtos, int first, int last, Long facultyNumber) {
        if (last >= first) {
            int mid = first + (last - first) / 2;

            if (recordDtos.get(mid).getFacultyNumber().equals(facultyNumber)) {
                return mid;
            }

            if (recordDtos.get(mid).getFacultyNumber() > facultyNumber) {
                return binarySearch(recordDtos, first, mid - 1, facultyNumber);
            }

            return binarySearch(recordDtos, mid + 1, last, facultyNumber);
        }

        return -1;
    }

    private double getAverageGrade(Student student) {
        return student.getGrades()
                .stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

}
