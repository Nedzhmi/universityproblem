package dss.university.service;

import dss.university.model.dto.StudentExportDto;
import dss.university.model.dto.StudentImportGradesDto;
import dss.university.model.dto.StudentRecordDto;
import dss.university.model.entity.Student;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface StudentService {

    Student addStudent(Student student);

    List<Student> getAll();

    Optional<Student> getById(Long id);

    Optional<Student> findByFacultyNumber(Long facultyNumber);

    boolean update(Long id, Student student);

    void delete(Long id);

    boolean addGradesToStudent(StudentImportGradesDto studentImportGradesDto);

    List<StudentExportDto> getAllWithAverageGrade();

    List<StudentExportDto> getAllSortedByFacultyNumber();

    List<StudentExportDto> getAllSortedByAverageGrade();

    StudentRecordDto getStudentRecord(Long facultyNumber);

    void writeToFile() throws IOException;

    List<Student> readFromFile() throws IOException;
}
