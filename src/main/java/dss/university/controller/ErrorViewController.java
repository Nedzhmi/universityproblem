package dss.university.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorViewController implements ErrorController {

    private static final String PATH = "/error";
    private static final String ERROR_PATH = "errors/error";

    @RequestMapping(PATH)
    public ModelAndView handleError() {
        return new ModelAndView(ERROR_PATH);
    }
}
