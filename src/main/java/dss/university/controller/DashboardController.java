package dss.university.controller;

import dss.university.model.dto.StudentGetRecordDto;
import dss.university.model.dto.StudentImportDto;
import dss.university.model.dto.StudentImportGradesDto;
import dss.university.model.entity.Student;
import dss.university.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping
public class DashboardController {

    private final StudentService studentService;
    private final ModelMapper modelMapper;

    @Autowired
    public DashboardController(StudentService studentService, ModelMapper modelMapper) {
        this.studentService = studentService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(path = {"/", "/index", "/add-student"})
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("add-student");

        if(!modelAndView.getModel().containsKey("student")){
            modelAndView.addObject("student", new StudentImportDto());
        }

        return modelAndView;
    }

    @PostMapping(path = "/add-student", consumes = {"application/x-www-form-urlencoded;charset=UTF-8"})
    public ModelAndView addStudentComplete(@ModelAttribute Student student, RedirectAttributes redirectAttributes,
                                    BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView("redirect:/add-student");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("student", student);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.homeworkAddBindingModel", bindingResult);
            return modelAndView;
        }
        student.setDeleted(0);
        this.studentService.addStudent(student);

        return modelAndView;
    }

    @GetMapping(path = "/add-grades")
    public ModelAndView addGrades() {
        ModelAndView modelAndView = new ModelAndView("add-grades");

        if(!modelAndView.getModel().containsKey("importData")){
            modelAndView.addObject("importData", new StudentImportGradesDto());
        }
        modelAndView.addObject("student", null);
        modelAndView.addObject("students", this.studentService.getAll());

        return modelAndView;
    }

    @PostMapping(path = "/add-grades", consumes = {"application/x-www-form-urlencoded;charset=UTF-8"})
    public ModelAndView addGradesComplete(@ModelAttribute StudentImportGradesDto student, RedirectAttributes redirectAttributes,
                                          BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView("add-grades");
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("student", student);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.homeworkAddBindingModel", bindingResult);
            return modelAndView;
        }

        this.studentService.addGradesToStudent(student);
        modelAndView.addObject("students", this.studentService.getAll());
        modelAndView.addObject("importData", student);

        return modelAndView;
    }

    @GetMapping(path = "/print-students-with-average-grade")
    public ModelAndView printStudentsWithAverageGrade() {
        ModelAndView modelAndView = new ModelAndView("print-students-with-average-grade");
        modelAndView.addObject("students", this.studentService.getAllWithAverageGrade());

        return modelAndView;
    }

    @GetMapping(path = "write-to-file")
    public ModelAndView writeToFile() throws IOException {
        ModelAndView modelAndView = new ModelAndView("redirect:/add-student");
        this.studentService.writeToFile();

        return modelAndView;
    }

    @GetMapping(path = "/read-from-file")
    public ModelAndView readFromFile() throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("students", this.studentService.readFromFile());

        if(modelAndView.getModel().get("students") != null) {
            modelAndView.setViewName("read-from-file");
        } else {
            modelAndView.setViewName("redirect:/add-student");
        }
        return modelAndView;
    }

    @GetMapping(path = "/sort-by-faculty-number")
    public ModelAndView sortByFacultyNumber() {
        ModelAndView modelAndView = new ModelAndView("sort-by-faculty-number");
        modelAndView.addObject("students", this.studentService.getAllSortedByFacultyNumber());

        return modelAndView;
    }

    @GetMapping(path = "/sort-by-average-grade")
    public ModelAndView sortByAverageGrade() {
        ModelAndView modelAndView = new ModelAndView("sort-by-average-grade");
        modelAndView.addObject("students", this.studentService.getAllSortedByAverageGrade());

        return modelAndView;
    }

    @GetMapping(path = "/get-student-record")
    public ModelAndView getStudentRecord(@ModelAttribute StudentGetRecordDto studentGetRecordDto,
                                         HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView("get-student-record");

        if(!modelAndView.getModel().containsKey("student")){
            modelAndView.addObject("student", null);
        }

        String strResult = request.getParameter("faculty_number");
        if (!"".equals(strResult) && strResult != null) {
            Long facNumb = Long.parseLong(strResult);
            modelAndView.addObject("student", this.studentService.getStudentRecord(facNumb));
        }

        modelAndView.addObject("students", this.studentService.getAll());

        return modelAndView;
    }

}
